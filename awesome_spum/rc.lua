-- Require: ImageMagick
local gears = require("gears")
local awful = require("awful")
awful.rules = require("awful.rules")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local menubar = require("menubar")
require("awful.autofocus")
require("menu")

os.execute ("xsetroot -solid '#000000' &")
os.execute ("xmodmap /home/rase/.Xmodmap &")
os.execute ("wicd-gtk &")

terminal   = "urxvt"
terminalex = "urxvt -e screen -U"
terminalbig = "urxvt -font '-*-terminus-medium-r-normal-*-24-*-*-*-*-*-iso10646-*' -e screen -U"
modkey     = "Mod4"

homedir = os.getenv('HOME')
confdir = homedir .. "/.config/awesome"

vol_up='5%+'
vol_dn='5%-'
vol_proto='amixer set %s "%s" | tr "\\n" "-" | sed --silent \'s/.*\\(\\[[0-9]*%%\\]\\).*\\(\\[[0-9]*%%\\]\\).*/%s: \\1 \\2\\n/gp\' | dzen2 -p 2 -x 412 -w 200 -sa c &'
vol_pcm_up=string.format(vol_proto, 'PCM', vol_up, 'PCM')
vol_pcm_dn=string.format(vol_proto, 'PCM', vol_dn, 'PCM')
vol_master_up=string.format(vol_proto, 'Master', vol_up, 'MASTER')
vol_master_dn=string.format(vol_proto, 'Master', vol_dn, 'MASTER')


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Error!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init( confdir .. "/theme.lua" )

-- This is used later as the default terminal and editor to run.
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor


function setstatus()
	textbox_ppp.text = execo ('echo -n "oo" `mpc --format "[%file%]" | tr "\\n" "^" | sed --silent \'s/.*\\/\\(.*\\)^.*#\\([0-9]*\\/[0-9]*\\)\\s*\\(.*\\)\\s(.*/\\2: \\3: \\1 |/p\'`; echo " `/sbin/ifconfig eth1 | sed \'s/.*bytes:\\([0-9]*\\) (\\([0-9.]*\\) \\(.\\).*bytes:\\([0-9]*\\) (\\([0-9.]*\\) \\(.\\).*/N:\\1-\\2\\3\\/\\4-\\5\\6   /p\' --silent`"; ~/.config/awesome/powerm.rb')
--	textbox_ppp.text = execo ('echo -n `mpc --format "[%file%]" | tr "\\n" "^" | sed --silent \'s/.*\\/\\(.*\\)^.*#\\([0-9]*\\/[0-9]*\\)\\s*\\(.*\\)\\s(.*/\\2: \\3: \\1 |/p\'`; echo " `/sbin/ifconfig ppp0 | sed \'s/.*bytes:\\([0-9]*\\) (\\([0-9.]*\\) \\(.\\).*bytes:\\([0-9]*\\) (\\([0-9.]*\\) \\(.\\).*/N:\\1-\\2\\3\\/\\4-\\5\\6   /p\' --silent`"; echo " `acpi`" | sed \'s/Battery 0: \\([a-zA-Z]*\\), \\([0-9]*%\\)/B:\\2\\1/; s/Charging/↑/; s/Discharging/↓/; s/, //g; s/z*[a-z A-Z]*$/ /\'')
--	textbox_ppp.text = execo ('echo -n `mpc --format "[%file%]" | tr "\\n" "^" | sed --silent \'s/.*\\/\\(.*\\)^.*#\\([1-9]*\\/[0-9]*\\)\\s*\\(.*\\)\\s(.*/\\2: \\3: \\1 |/p\'`; echo " `acpi`" | sed \'s/Battery 0: \\([a-zA-Z]*\\), \\([0-9]*%\\)/B:\\2\\1/; s/Charging/↑/; s/Discharging/↓/; s/, //g; s/z*[a-z A-Z]*$/ /\'')
end

function execo(command)
   local fh = io.popen(command)
   local str = ""
   for i in fh:lines() do
      str = str .. i
   end
   io.close(fh)
   return str
end


function match (table1, table2)
   for k, v in pairs(table1) do
      if table2[k] ~= v and not table2[k]:find(v) then
         return false
      end
   end
   return true
end


function run_or_raise(cmd, properties)
   local clients = client.get()
   local focused = awful.client.next(0)
   local findex = 0
   local matched_clients = {}
   local n = 0
   for i, c in pairs(clients) do
      if match(properties, c) then
         n = n + 1
         matched_clients[n] = c
         if c == focused then
            findex = n
         end
      end
   end
   if n > 0 then
      local c = matched_clients[1]
      if 0 < findex and findex < n then
         c = matched_clients[findex+1]
      end
      local ctags = c:tags()
      if table.getn(ctags) == 0 then
         local curtag = awful.tag.selected()
         awful.client.movetotag(curtag, c)
      else
         awful.tag.viewonly(ctags[1])
      end
      client.focus = c
      c:raise()
      return
   end
   awful.util.spawn(cmd)
end

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    awful.layout.suit.max,
}
-- }}}

-- {{{ Wallpaper
--if beautiful.wallpaper then
--    for s = 1, screen.count() do
--        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
--    end
-- end
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }, s, layouts[1])
end
-- }}}

menu_main = awful.menu({ items = { 
  { "ALT", menu.alt_menu },
  { "Terminal", terminalex },
  { "Sys: hibernate", function () naughty.notify({text=execo("sudo pm-hibernate"), timeout=10 }) end },
  { "Sys: reboot", function () naughty.notify({text=execo("sudo init 6"), timeout=10 }) end },
  { "Sys: shutdown", function () naughty.notify({text=execo("sudo halt"), timeout=10 }) end },
  { "Awe: restart", awesome.restart },
  { "Awe: quit", awesome.quit } 
} })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = menu_main })
-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock(" %Y.%m.%d.%u   %H:%M ")
mytext_power = wibox.widget.textbox()
mytext_power:set_markup(execo( homedir ..  '.config/awesome/powerm.rb')) 

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget & remove unused tags
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.noempty, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylauncher)
    left_layout:add(mytaglist[s])
    left_layout:add(mypromptbox[s])

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(mytext_power)
    right_layout:add(mytextclock)
    if s == 1 then right_layout:add(wibox.widget.systray()) end
    right_layout:add(mylayoutbox[s])

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytasklist[s])
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
--    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "Up",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "Down",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
--    awful.key({ modkey,           }, "a", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "Up", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Shift"   }, "Down", function () awful.client.swap.byidx( -1)    end),
--    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
--    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey, "Control" }, "n", awful.client.restore),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminalex) end),
    awful.key({ modkey, "Shift"   }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "Return", function () awful.util.spawn(terminalbig) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),


    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey,           }, "Right", function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "Left",  function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey, "Control" }, "space", function () awful.layout.inc(layouts, 1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, 1) end),

    awful.key({ modkey },            "p",     function () os.execute( confdir .. "/proglist") end),
    awful.key({ modkey },            "[",     function () os.execute( confdir .. "/mntumnt &") end),
    awful.key({ modkey },            "]",     function () os.execute( confdir .. "/mntumnt --umount &") end),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),


    awful.key({ modkey,           }, "f",     function () run_or_raise( "iceweasel", { class = "Iceweasel" } ) end),
				-- #3
    awful.key({ modkey,           }, "i",     function () run_or_raise( "pidgin", { class = "Pidgin" } ) end),
    				-- #9
    awful.key({ modkey,           }, "y",     function () run_or_raise( "/usr/sbin/synaptic", { class = "Synaptic" } ) end),
    awful.key({ modkey,           }, "t",     function () run_or_raise( "deluge", { class="Deluge"} ) end),
    awful.key({ modkey,           }, "s",     function () os.execute( "stardict &") end),
    awful.key({ },                   "Print", function () os.execute( "import -window root `cat ~/.screenshots``date +%Y%m%d-%H%M%S`.png &" ) end),
    awful.key({ modkey },            "m",     function () os.execute( terminal .. " -e alsamixer &") end),
    awful.key({ modkey },            "d",     function () os.execute( terminal .. " -e ncmpc &") end),

    awful.key({ modkey },            "c",     function () os.execute( "mpc toggle &") end),
    awful.key({ modkey, "Shift" },   "c",     function () os.execute( "mpc stop &") end),
    awful.key({ modkey },            "z",     function () os.execute( "mpc prev &") end),
    awful.key({ modkey },            "x",     function () os.execute( "mpc next &") end),
    awful.key({ modkey, "Shift" },   "z",     function () os.execute( "mpc seek -00:00:10 &") end),
    awful.key({ modkey, "Control" }, "z",     function () os.execute( "mpc seek -00:01:00 &") end),
    awful.key({ modkey, "Shift" },   "x",     function () os.execute( "mpc seek +00:00:10 &") end),
    awful.key({ modkey, "Control" }, "x",     function () os.execute( "mpc seek +00:01:00 &") end),

    awful.key({ },     "XF86AudioRaiseVolume",function () os.execute(vol_pcm_up) end),
    awful.key({ },     "XF86AudioLowerVolume",function () os.execute(vol_pcm_dn) end),
    awful.key({modkey},"XF86AudioRaiseVolume",function () os.execute(vol_master_up) end),
    awful.key({modkey},"XF86AudioLowerVolume",function () os.execute(vol_master_dn) end),

    awful.key({ modkey, "Control" }, "i",
    function (c)
	    local class = ""
	    local name = ""
	    local instance = ""

	    if client.focus.class then
		class = client.focus.class
	    end
	    if client.focus.name then
		name = client.focus.name
	    end
	    if client.focus.instance then
		instance = client.focus.instance
	    end

	  naughty.notify({
	    text="c: " .. class .. " i: " .. instance,
	    title=name,
	    timeout=10 })

       end),
    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey, "Control" }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift" },   "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Control" }, "q",      function (c) c:kill()                         end),
    awful.key({ modkey            }, "q",      function (c) c:kill()                         end),

    awful.key({ modkey            }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "space",  function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
-- old version:    awful.key({ modkey,           }, "n",      function (c) c.minimized = not c.minimized    end)

    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Compute the maximum number of digit we need, limited to 9
-- keynumber = 0
-- for s = 1, screen.count() do
--   keynumber = math.min(9, math.max(#tags[s], keynumber))
-- end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
--                     focus = awful.client.focus.filter,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
     { rule = { class = "Vlc" },
       properties = { floating = true } },
     { rule = { class = "MPlayer" },
       properties = { floating = true } },
     { rule = { class = "Iceweasel" },
       properties = { tag = tags[1][2] } },
     { rule = { class = "Pidgin" },
       properties = { tag = tags[1][3] } },
     { rule = { class = "Wine" },
       properties = { tag = tags[1][4] } },
     { rule = { class = "Deluge" },
       properties = { tag = tags[1][7] } },
     { rule = { class = "Gimp" },
       properties = { tag = tags[1][8], floating=true } },
     { rule = { class = "Synaptic" },
       properties = { tag = tags[1][9] } },
     { rule = { class = "guiswing-ClientForm" },
       properties = { tag = tags[1][10] } },
    { rule = { instance = "plugin-container" },
        properties = { floating = true } },
}





-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local title = awful.titlebar.widget.titlewidget(c)
        title:buttons(awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                ))

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(title)

        awful.titlebar(c):set_widget(layout)

        -- Honor size hints
        c.size_hints_honor = false
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
